import 'dart:io';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myapp/wig.dart';
import 'package:flutter_tts/flutter_tts.dart';


void main() =>
  runApp(MaterialApp(  debugShowCheckedModeBanner: false,

      home:  Home() ));


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  File _image;
  String clr ="1",q='myapp',a="",z="";
  TextEditingController myController = TextEditingController();
  final FlutterTts tts = FlutterTts();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(q),
        centerTitle: true,
        backgroundColor: Colors.cyanAccent[400],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 12.0,),

            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: TextField(
                controller:  myController ,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)
                    ),

                    hintText: 'Enter your name'
                ),
              ),
            ),
            SizedBox(height: 12.0,),


               Padding(
                 padding: const EdgeInsets.symmetric(horizontal: 125),
                 child: RadioButtonGroup(
                    orientation: GroupedButtonsOrientation.HORIZONTAL,
            margin: const EdgeInsets.symmetric(),

          labels: <String>[
                      "male",
                      "female",
                      "other",
                    ],
                    onSelected: (String selected) => z=selected),
               ),

            Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
              child: RaisedButton.icon(
                  onPressed: ()async{
                    myController.text != ""?z=="male"?a= myController.text+" the king":z=="female"?a= myController.text+" the queen":a= myController.text+" the gay":a="";

                    myController.text != ""?q=myController.text+"'s app":q="my app";

                    await tts.setLanguage("en-US");
                    await tts.speak(a);
                    setState(() {
                      clr ="";


                    });
                  },
                  icon: Icon(
                    Icons.confirmation_number
                  ),
                  label: Text(clr),
                  ),
            ),
            SizedBox(height:25,),
            Container(
              child: Text(a,
                style: TextStyle(
                  color: Colors.white,
                      backgroundColor: Colors.black,
                      fontSize: 50
                ),),
            ),
            Container(

              width: 150,
              child:_image != null? Image.file(_image,fit: BoxFit.fitWidth):Image.network("https://www.uh.edu/images/interlocking-uh.png"),
            ),
            SizedBox(height: 10,),

            ],

        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed:()async{

            clr = clr+"1";
            // ignore: deprecated_member_use
            var image = await  ImagePicker.pickImage(
                source: ImageSource.gallery, imageQuality: 50);

            setState(()  { _image =image;
            // Navigator.of(context).pop();

            }); },
        child: Center(child: Text("add image",
        style: TextStyle(fontSize: 10),)),
        backgroundColor: Colors.amber,

      ),
      backgroundColor: Colors.white,

    );



  }
}
